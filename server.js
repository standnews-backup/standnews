const express = require("express");
const axios = require("axios");
const cors = require("cors");
const system = require("system-commands");
const app = express();
app.use(cors());
app.use(function (req, res, next) {
  if (req.path.split("/")[1] === "api") {
    res.setHeader("Content-Type", "application/json");
  } else if (req.path.indexOf(".") === -1) {
    res.setHeader("Content-Type", "text/html");
  }
  next();
});
app.use("/", express.static(__dirname));
app.use("/", express.static(__dirname + "/index.html"));
app.use(function (req, res) {
  axios
    .get(
      "https://archive.org/wayback/available?url=www.thestandnews.com" +
        req.path
    )
    .then(async (res1) => {
      try {
        if (!Object.keys(res1.data.archived_snapshots).length) {
          let r = false;
          await axios
            .get(
              "https://gitlab.com/standnews-backup/www.thestandnews.com/-/raw/master" +
                req.path
            )
            .then(() => {})
            .catch(() => {
              res.status(404);
              res.send("file not available here and on archive.org");
              r = true;
            });
          if (r) return;
        }
        let path = req.path.replace("/", ""),
          useindexhtml = false;
        if (res.getHeader("Content-Type") == "text/html") useindexhtml = true;
        if (req.path.endsWith("/")) path = path.slice(0, -1);
        let filename = path.split("/").pop();
        path = path.replace(filename, "");
        filename = decodeURIComponent(filename);
        if (useindexhtml) path += filename + "/";
        const cmd = `${path ? "mkdir -p " + path + " && " : ""}curl ${
          res1?.data?.archived_snapshots?.closest?.url?.replace(
            "/https://www.thestandnews.com",
            "id_/https://www.thestandnews.com"
          ) ||
          "https://gitlab.com/standnews-backup/www.thestandnews.com/-/raw/master" +
            req.path
        } -o ${path ? path : ""}${useindexhtml ? "index.html" : filename}`;
        console.log(cmd);
        try {
          await system(cmd);
        } catch (e) {
          console.log(e);
        }
        await system(
          `sed -i "s/https:\\/\\/assets.thestandnews.com/http:\\/\\/localhost:2998/g" ${
            path ? path : ""
          }${useindexhtml ? "index.html" : filename}`
        );
        res.redirect("/" + path + useindexhtml ? "" : filename);
      } catch (e) {
        res.status(503);
        res.send(e);
      }
    });
});
app.listen(2999);
