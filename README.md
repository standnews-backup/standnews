# About
A lightweight script to get Stand News files on-demand from internet archive.<br>
Nothing is downloaded unless it is requested.
So it would be slow if the file hasn't been downloaded.

If you want at least some pre-downloaded htmls use [standnews-backup/www.thestandnews.com](https://gitlab.com/standnews-backup/www.thestandnews.com).
# Deployment
```
npm install
node server.js
```
The server will start at localhost:2999.<br>

If you want to serve with images also deploy [standnews-assets](https://gitlab.com/standnews-backup/standnews-assets).
Edit server.js if your assets instance is not at localhost:2998.
